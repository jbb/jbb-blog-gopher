use std::net::{TcpListener, TcpStream, SocketAddr};
use std::io::BufReader;
use std::io::BufRead;

use std::fs::read_dir;
use std::fs::DirEntry;
use std::fs::read_to_string;
use std::path::Path;

use serde_derive::*;

use gophermap::GopherMenu;
use gophermap::ItemType;

use textwrap;

const HOSTNAME: &'static str = "jbb.ghsq.de";
const PORT: u16 = 70;

fn log(stream: &TcpStream) {
    stream.peer_addr().and_then(|addr: SocketAddr| {
        println!("Request from {}", addr.ip());
        Ok(())
    }).or_else(|_e: std::io::Error| {
        println!("Request from unknown ip address");
        Err(())
    }).ok();
}

#[derive(Deserialize, Default)]
struct PostMeta {
    title: String,
    date: Option<String>,
    created_at: Option<String>
}

#[derive(Deserialize, Default)]
struct Post {
    meta: PostMeta,
    content: String,
}

fn parse_post(content: &str) -> Post {
    let all_parts: Vec<&str> = content.split("---").collect::<Vec<&str>>();
    let mut parts: Vec<&str> = Vec::new();
    for part in all_parts {
        if !part.is_empty() {
            parts.push(part)
        }
    }

    let yaml = parts.get(0).unwrap_or(&"");
    let article_meta: PostMeta = serde_yaml::from_str(yaml).unwrap();

    // Remove meta part
    if parts.len() > 0 {
        parts.remove(0);
    }

    Post {
        meta: article_meta,
        content: parts.join("---")
    }
}

fn gopher_respond(stream: &mut TcpStream, line: &str) {
    let mut menu = GopherMenu::with_write(stream);

    match line {
        "/" | "" => {
            menu.info("=====").unwrap();
            menu.info("Posts").unwrap();
            menu.info("=====").unwrap();
            menu.info("").unwrap();

            match read_dir(Path::new("_posts")) {
                Ok(it) => {
                    let mut files: Vec<DirEntry> = Vec::new();
                    for result in it {
                        match result {
                            Ok(entry) => files.push(entry),
                            Err(_e) => continue
                        }
                    }

                    files.sort_by(|dir_entry_a, dir_entry_b| {
                        if dir_entry_a.file_name() < dir_entry_b.file_name() {
                            std::cmp::Ordering::Greater
                        } else {
                            std::cmp::Ordering::Less
                        }
                    });

                    for entry in files {
                        let entry: DirEntry = entry;
                        let os_file_name = entry.file_name();

                        let name = os_file_name.to_str().unwrap_or("unknown file");
                        let content = read_to_string(entry.path()).unwrap_or_default();

                        let post = parse_post(&content);
                        menu.write_entry(ItemType::Directory, &post.meta.title, &name, &HOSTNAME, PORT).expect("Failed to add file to menu");

                        let mut line_n = 0;
                        for line in textwrap::fill(&post.content, 80).split("\n") {
                            // Skip empty lines
                            if line.trim().is_empty() {
                                continue;
                            }

                            // Only take for lines
                            if line_n < 5 {
                                menu.info(line).unwrap();
                                line_n += 1;
                                continue;
                            }

                            menu.info("").unwrap();

                            break;
                        }
                    }
                },
                Err(error) => {
                    println!("Can't open directory _posts: {}", error);
                }
            }
        },
        path => {
            if !path.contains("/") && !path.contains("..") {
                match read_to_string(&format!("_posts/{}", path)) {
                    Ok(file) => {
                        let post = parse_post(&file);

                        menu.info("==================================").unwrap();
                        menu.info(&post.meta.title).unwrap();
                        menu.info("==================================").unwrap();
                        menu.info("").unwrap();
                        if post.meta.date.is_some() {
                            menu.info(&format!("Published on {}", post.meta.date.unwrap())).unwrap();
                        }

                        for line in textwrap::fill(&post.content, 80).split("\n") {
                            // TODO:
                            // * extract images, add them as image element
                            // * extract links, add them as link element
                            menu.info(line).unwrap();
                        }
                    },
                    Err(_e) =>  {
                        menu.write_entry(ItemType::Error, "Page not found :(", "", &HOSTNAME, PORT).unwrap();
                        menu.info(&format!("Tried: {}", path)).unwrap();
                    }
                }
            } else {
                menu.write_entry(ItemType::Error, "You tried to visit a forbidden directory", "", &HOSTNAME, PORT).unwrap();
            }
        },
    }

    menu.end().unwrap();
}


fn handle_client(mut stream: TcpStream) {
    // Logging
    log(&stream);

    let mut line = String::new();
    BufReader::new(&stream).read_line(&mut line).unwrap();
    let line = line.trim();

    gopher_respond(&mut stream, line);
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("0.0.0.0:7000")?;

    // accept connections and process them serially
    for stream in listener.incoming() {
        handle_client(stream?);
    }
    Ok(())
}
